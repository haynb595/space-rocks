{
    "id": "fc0fbd34-03ca-4d8f-835d-8daf1f914df5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "e617fffa-68cc-4a12-a91f-228100e9889d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "fc0fbd34-03ca-4d8f-835d-8daf1f914df5"
        },
        {
            "id": "4fdf1de8-1e4b-4fc6-9bb8-d83afa82b4f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "fc0fbd34-03ca-4d8f-835d-8daf1f914df5"
        },
        {
            "id": "219b0986-b6ca-4a9e-aabb-0a4409be70aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "fc0fbd34-03ca-4d8f-835d-8daf1f914df5"
        },
        {
            "id": "1385ebec-36de-4446-acd2-574b9feef7bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fc0fbd34-03ca-4d8f-835d-8daf1f914df5"
        },
        {
            "id": "fc52c92c-1872-4fec-aaa8-caa208aa9d68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "31faa8e9-4f2e-43cd-ae1b-aafaa16c76ec",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fc0fbd34-03ca-4d8f-835d-8daf1f914df5"
        },
        {
            "id": "f89782e9-61ae-411e-aa3f-ca286a4fc836",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "fc0fbd34-03ca-4d8f-835d-8daf1f914df5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8bb5a007-265e-470b-9d83-39e83de91424",
    "visible": true
}