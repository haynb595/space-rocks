/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 66100244
/// @DnDApplyTo : c7a915b7-1b15-405f-9c27-c76c317e8af3
with(obj_game) {
	/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
	/// @DnDVersion : 1
	/// @DnDHash : 018D39F8
	/// @DnDParent : 66100244
	/// @DnDArgument : "lives" "-1"
	/// @DnDArgument : "lives_relative" "1"
	if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
	__dnd_lives += real(-1);
}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 43A6D000
instance_destroy();

/// @DnDAction : YoYo Games.Loops.Repeat
/// @DnDVersion : 1
/// @DnDHash : 7E5524D5
/// @DnDArgument : "times" "10"
repeat(10)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 64151CC7
	/// @DnDParent : 7E5524D5
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_debris"
	/// @DnDSaveInfo : "objectid" "f858ba6b-8526-48d0-9ae2-f8f5fd48df43"
	instance_create_layer(x + 0, y + 0, "Instances", obj_debris);

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 555CDBAC
	/// @DnDParent : 7E5524D5
	/// @DnDArgument : "xpos" "200"
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos" "200"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_ship"
	/// @DnDSaveInfo : "objectid" "fc0fbd34-03ca-4d8f-835d-8daf1f914df5"
	instance_create_layer(x + 200, y + 200, "Instances", obj_ship);
}