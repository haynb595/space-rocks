/// @DnDAction : YoYo Games.Instance Variables.Set_Score
/// @DnDVersion : 1
/// @DnDHash : 48DAD654

__dnd_score = real(0);

/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 5D133DBF
/// @DnDArgument : "lives" "3"

__dnd_lives = real(3);

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 5D34D1C7
/// @DnDArgument : "font" "font_text"
/// @DnDSaveInfo : "font" "67ba3603-fc74-4331-8cdd-1c6cb59a197e"
draw_set_font(font_text);