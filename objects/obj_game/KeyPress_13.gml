/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 687F73C0
/// @DnDArgument : "expr" "room"
var l687F73C0_0 = room;
switch(l687F73C0_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 1CF5F21A
	/// @DnDParent : 687F73C0
	/// @DnDArgument : "const" "room_start"
	case room_start:
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 3777804E
		/// @DnDParent : 1CF5F21A
		/// @DnDArgument : "room" "room_game"
		/// @DnDSaveInfo : "room" "6023ff5e-2eed-48a2-8f79-53c0b4770cb1"
		room_goto(room_game);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 1998564C
	/// @DnDParent : 687F73C0
	/// @DnDArgument : "const" "room_gameover"
	case room_gameover:
		/// @DnDAction : YoYo Games.Game.Restart_Game
		/// @DnDVersion : 1
		/// @DnDHash : 3811BB7E
		/// @DnDParent : 1998564C
		game_restart();
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 7FEBB812
	/// @DnDParent : 687F73C0
	/// @DnDArgument : "const" "room_wingame "
	case room_wingame :
		/// @DnDAction : YoYo Games.Game.Restart_Game
		/// @DnDVersion : 1
		/// @DnDHash : 09DDA3D3
		/// @DnDParent : 7FEBB812
		game_restart();
		break;
}