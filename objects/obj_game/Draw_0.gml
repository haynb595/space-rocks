/// @DnDAction : YoYo Games.Switch.Switch
/// @DnDVersion : 1
/// @DnDHash : 744ACC30
/// @DnDArgument : "expr" "room"
var l744ACC30_0 = room;
switch(l744ACC30_0)
{
	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 5ACF61C3
	/// @DnDParent : 744ACC30
	/// @DnDArgument : "const" "room_game"
	case room_game:
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
		/// @DnDVersion : 1
		/// @DnDHash : 5348DBA8
		/// @DnDParent : 5ACF61C3
		/// @DnDArgument : "x" "20"
		/// @DnDArgument : "y" "20"
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		draw_text(20, 20, string("Score: ") + string(__dnd_score));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Lives
		/// @DnDVersion : 1
		/// @DnDHash : 04E3FDBE
		/// @DnDParent : 5ACF61C3
		/// @DnDArgument : "x" "20"
		/// @DnDArgument : "y" "40"
		/// @DnDArgument : "sprite" "spr_lives"
		/// @DnDSaveInfo : "sprite" "588b3612-c3bc-4fb1-b5b5-20940655e4d8"
		var l04E3FDBE_0 = sprite_get_width(spr_lives);
		var l04E3FDBE_1 = 0;
		if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
		for(var l04E3FDBE_2 = __dnd_lives; l04E3FDBE_2 > 0; --l04E3FDBE_2) {
			draw_sprite(spr_lives, 0, 20 + l04E3FDBE_1, 40);
			l04E3FDBE_1 += l04E3FDBE_0;
		}
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 7974C1BC
	/// @DnDParent : 744ACC30
	/// @DnDArgument : "const" "room_start"
	case room_start:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 3ACCBBFF
		/// @DnDParent : 7974C1BC
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 0F60D3FD
		/// @DnDParent : 7974C1BC
		/// @DnDArgument : "color" "$FF82FF88"
		draw_set_colour($FF82FF88 & $ffffff);
		var l0F60D3FD_0=($FF82FF88 >> 24);
		draw_set_alpha(l0F60D3FD_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 16C3CD71
		/// @DnDParent : 7974C1BC
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "100"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""SPACE ROCKS:""
		draw_text_transformed(300, 100, string("SPACE ROCKS:") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 251CCEE0
		/// @DnDParent : 7974C1BC
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "150"
		/// @DnDArgument : "xscale" "3"
		/// @DnDArgument : "yscale" "3"
		/// @DnDArgument : "caption" ""THE TURTLE ARCH ""
		draw_text_transformed(300, 150, string("THE TURTLE ARCH ") + "", 3, 3, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 02F6053E
		/// @DnDParent : 7974C1BC
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "200"
		/// @DnDArgument : "caption" ""Score 1000 or more points to win! ""
		draw_text(300, 200, string("Score 1000 or more points to win! ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 442E30C5
		/// @DnDParent : 7974C1BC
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "230"
		/// @DnDArgument : "caption" ""Move with UP/LEFT/RIGHT keys""
		draw_text(300, 230, string("Move with UP/LEFT/RIGHT keys") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 0ECD1C03
		/// @DnDParent : 7974C1BC
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "250"
		/// @DnDArgument : "caption" ""Press SPACE to shoot turtle bullets ""
		draw_text(300, 250, string("Press SPACE to shoot turtle bullets ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 54FC8143
		/// @DnDParent : 7974C1BC
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "270"
		/// @DnDArgument : "caption" "">>PRESS ENTER TO START<<""
		draw_text(300, 270, string(">>PRESS ENTER TO START<<") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 3988636B
		/// @DnDParent : 7974C1BC
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 0582896B
	/// @DnDParent : 744ACC30
	/// @DnDArgument : "const" " room_gameover"
	case  room_gameover:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 35069D3D
		/// @DnDParent : 0582896B
		/// @DnDArgument : "halign" "fa_center"
		/// @DnDArgument : "valign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_center);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 7F998550
		/// @DnDParent : 0582896B
		/// @DnDArgument : "color" "$FF6005FF"
		draw_set_colour($FF6005FF & $ffffff);
		var l7F998550_0=($FF6005FF >> 24);
		draw_set_alpha(l7F998550_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 1D00FB43
		/// @DnDParent : 0582896B
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "300"
		/// @DnDArgument : "xscale" "4"
		/// @DnDArgument : "yscale" "4"
		/// @DnDArgument : "caption" ""THE TURTLE""
		draw_text_transformed(300, 300, string("THE TURTLE") + "", 4, 4, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 0645C4FD
		/// @DnDParent : 0582896B
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "350"
		/// @DnDArgument : "xscale" "4"
		/// @DnDArgument : "yscale" "4"
		/// @DnDArgument : "caption" ""ARCH LOSES ""
		draw_text_transformed(300, 350, string("ARCH LOSES ") + "", 4, 4, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 1CEAC5C5
		/// @DnDParent : 0582896B
		/// @DnDArgument : "color" "$FFFF492D"
		draw_set_colour($FFFF492D & $ffffff);
		var l1CEAC5C5_0=($FFFF492D >> 24);
		draw_set_alpha(l1CEAC5C5_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
		/// @DnDVersion : 1
		/// @DnDHash : 66657A61
		/// @DnDParent : 0582896B
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "400"
		/// @DnDArgument : "caption" ""Final Score ""
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		draw_text(300, 400, string("Final Score ") + string(__dnd_score));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 30EF182C
		/// @DnDParent : 0582896B
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "425"
		/// @DnDArgument : "caption" "">>PRESS ENTER TO RESTART ""
		draw_text(300, 425, string(">>PRESS ENTER TO RESTART ") + "");
	
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 75F71982
		/// @DnDParent : 0582896B
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		break;

	/// @DnDAction : YoYo Games.Switch.Case
	/// @DnDVersion : 1
	/// @DnDHash : 34378879
	/// @DnDParent : 744ACC30
	/// @DnDArgument : "const" "room_wingame"
	case room_wingame:
		/// @DnDAction : YoYo Games.Drawing.Set_Alignment
		/// @DnDVersion : 1.1
		/// @DnDHash : 45E8DB89
		/// @DnDParent : 34378879
		/// @DnDArgument : "halign" "fa_center"
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 3B2ADC3B
		/// @DnDParent : 34378879
		/// @DnDArgument : "color" "$FFF5FF3A"
		draw_set_colour($FFF5FF3A & $ffffff);
		var l3B2ADC3B_0=($FFF5FF3A >> 24);
		draw_set_alpha(l3B2ADC3B_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 1DFDC919
		/// @DnDParent : 34378879
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "100"
		/// @DnDArgument : "caption" ""THE TURTLE""
		draw_text_transformed(300, 100, string("THE TURTLE") + "", 1, 1, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value_Transformed
		/// @DnDVersion : 1
		/// @DnDHash : 050DAA87
		/// @DnDParent : 34378879
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "150"
		/// @DnDArgument : "caption" ""ARCH WINS""
		draw_text_transformed(300, 150, string("ARCH WINS") + "", 1, 1, 0);
	
		/// @DnDAction : YoYo Games.Drawing.Set_Color
		/// @DnDVersion : 1
		/// @DnDHash : 4F2DFD50
		/// @DnDParent : 34378879
		/// @DnDArgument : "color" "$FFFF1E26"
		draw_set_colour($FFFF1E26 & $ffffff);
		var l4F2DFD50_0=($FFFF1E26 >> 24);
		draw_set_alpha(l4F2DFD50_0 / $ff);
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
		/// @DnDVersion : 1
		/// @DnDHash : 799B0684
		/// @DnDParent : 34378879
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "200"
		/// @DnDArgument : "caption" ""Final Score: ""
		if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
		draw_text(300, 200, string("Final Score: ") + string(__dnd_score));
	
		/// @DnDAction : YoYo Games.Drawing.Draw_Value
		/// @DnDVersion : 1
		/// @DnDHash : 0CF4618D
		/// @DnDParent : 34378879
		/// @DnDArgument : "x" "300"
		/// @DnDArgument : "y" "250"
		/// @DnDArgument : "caption" "">>PRESS ENTER TO RESTART<<""
		draw_text(300, 250, string(">>PRESS ENTER TO RESTART<<") + "");
		break;
}