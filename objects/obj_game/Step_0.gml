/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5322E909
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "room_game"
if(room == room_game)
{
	/// @DnDAction : YoYo Games.Instance Variables.If_Score
	/// @DnDVersion : 1
	/// @DnDHash : 2F25B55D
	/// @DnDParent : 5322E909
	/// @DnDArgument : "op" "4"
	/// @DnDArgument : "value" "1000"
	if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
	if(__dnd_score >= 1000)
	{
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 4CDC4164
		/// @DnDParent : 2F25B55D
		/// @DnDArgument : "room" "room_wingame"
		/// @DnDSaveInfo : "room" "822bbe9c-9ddf-4234-8559-033c9c517f34"
		room_goto(room_wingame);
	}

	/// @DnDAction : YoYo Games.Instance Variables.If_Lives
	/// @DnDVersion : 1
	/// @DnDHash : 3F50A26F
	/// @DnDParent : 5322E909
	/// @DnDArgument : "op" "3"
	if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
	if(__dnd_lives <= 0)
	{
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 0875E489
		/// @DnDParent : 3F50A26F
		/// @DnDArgument : "room" "room_gameover      "
		room_goto(room_gameover      );
	}
}