/// @DnDAction : YoYo Games.Random.Choose
/// @DnDVersion : 1
/// @DnDHash : 3FC86743
/// @DnDInput : 3
/// @DnDArgument : "var" "sprite_index"
/// @DnDArgument : "option" "spr_medium"
/// @DnDArgument : "option_1" "spr_large"
/// @DnDArgument : "option_2" "spr_small"
sprite_index = choose(spr_medium, spr_large, spr_small);

/// @DnDAction : YoYo Games.Random.Get_Random_Number
/// @DnDVersion : 1
/// @DnDHash : 1BCB9F12
/// @DnDArgument : "var" "direction"
/// @DnDArgument : "type" "1"
/// @DnDArgument : "max" "300"
direction = floor(random_range(0, 300 + 1));

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 0DB4034A
/// @DnDArgument : "speed" "1"
speed = 1;