/// @DnDAction : YoYo Games.Movement.Wrap_Room
/// @DnDVersion : 1
/// @DnDHash : 548A7872
/// @DnDArgument : "margin" "sprite_width / 2"
move_wrap(1, 1, sprite_width / 2);

/// @DnDAction : YoYo Games.Instances.Sprite_Rotate
/// @DnDVersion : 1
/// @DnDHash : 6E780A65
/// @DnDArgument : "angle" "2"
/// @DnDArgument : "angle_relative" "1"
image_angle += 2;