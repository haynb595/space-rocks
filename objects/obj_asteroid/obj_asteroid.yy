{
    "id": "31faa8e9-4f2e-43cd-ae1b-aafaa16c76ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_asteroid",
    "eventList": [
        {
            "id": "d8d49512-7b83-43dd-a500-904e72879175",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "31faa8e9-4f2e-43cd-ae1b-aafaa16c76ec"
        },
        {
            "id": "6a11251d-05d8-4619-8400-f61c604c8f1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "31faa8e9-4f2e-43cd-ae1b-aafaa16c76ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d498978e-e44a-4afa-b8cf-1cd46977e49e",
    "visible": true
}