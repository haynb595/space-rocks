{
    "id": "e97ee39b-b173-4787-9076-0dfe3e875029",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "df883050-505b-4017-91ef-bc9b0c57a5d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "31faa8e9-4f2e-43cd-ae1b-aafaa16c76ec",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e97ee39b-b173-4787-9076-0dfe3e875029"
        },
        {
            "id": "4f45e184-af31-4582-8bc3-7e9349f454dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "e97ee39b-b173-4787-9076-0dfe3e875029"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dac5c0c9-c4e1-4c45-ab72-fed1688a869d",
    "visible": true
}