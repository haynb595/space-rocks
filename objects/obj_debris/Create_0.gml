/// @DnDAction : YoYo Games.Random.Get_Random_Number
/// @DnDVersion : 1
/// @DnDHash : 55E02694
/// @DnDArgument : "var" "direction"
/// @DnDArgument : "max" "359"
direction = (random_range(0, 359));

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 63E49736
/// @DnDArgument : "speed" "8"
speed = 8;