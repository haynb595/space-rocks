{
    "id": "67ba3603-fc74-4331-8cdd-1c6cb59a197e",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_text",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Ink Free",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "55ae771b-8c89-4914-b69d-5c856ab5c35c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a60abff1-c4c1-44a9-a633-f75ea12c8f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 157,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e40e92ed-45a8-4112-b2a7-7727ff246a6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 151,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5b737b3f-acae-414c-bc6b-652a0a34137f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 138,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d449199d-e65d-41a9-b1e4-a672c9effa21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 127,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d5f935b3-4ad0-4817-aa0e-669b391d5f6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 109,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "dc6cb3b2-c0c3-40fc-a2fe-c781b576a37d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 96,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e1243791-885c-4a97-b8b3-7c73b6422e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 92,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8a5de454-227f-4c1e-874d-b09eee8ce77a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e1fb944c-6d37-4954-9af1-c42e6427590b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ef333573-ae01-49b2-b592-3b583d18a250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 161,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "124e2db9-4963-4512-97ff-8541d99ccb02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 68,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "196d2c19-6fc7-47f0-9857-742e346ecdc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 52,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3d909502-6172-4918-80aa-88ce28f55373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 45,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d2563199-3917-4b12-b6a7-343a9627788c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 41,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e3a96a58-813b-4aad-9dd1-6391d6bf1df1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 33,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b5cda250-a9f3-4cdb-810f-dbd3e1cd3266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 22,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ffcfc584-fc4c-4bd1-ab66-197d6a9927cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 14,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dff5fe28-ea6e-40eb-a3a9-b00f3d111e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0d7ea86c-e700-46b0-9861-d57ff7ad3370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 243,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c5bcbfa3-6f58-4394-86de-ffd8723879c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 230,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "df318585-61d2-4283-b14f-2b1bf56046e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "604c3f9b-9e78-4add-b632-86f9b7abb586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 169,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c725e0a8-f8cd-4db2-89d5-9e198a8bb255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 46
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "061d354e-9e1c-4420-b577-2dd8b03bdbfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 190,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "44be9205-1112-428d-80a6-1e4d6ffa5862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 182,
                "y": 68
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cfe92970-69d1-440d-8d2c-48cf1ea80078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 178,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "657e8862-6989-443c-9849-7bdd0b73ad54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 173,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7fa7a0b2-597b-406f-81d1-43ebfae0c007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 163,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9ec9ae82-ef6f-40b6-ad95-238731ea8df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 153,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d9ece470-436d-47b0-bef7-3ea92f47677c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 143,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "20808c36-6e41-440c-8822-b707728c9a8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 133,
                "y": 68
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8dfee2a2-709e-49bc-9ce2-555abcef6da4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 116,
                "y": 68
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "510e3825-e43e-47ce-a207-669ebe9daaf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 104,
                "y": 68
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "33409462-fe49-45bb-b574-3595d1092a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 90,
                "y": 68
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "85ba6df9-de08-4abe-b9c5-2723709a89ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 68
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2f2a7f62-053b-42b7-b324-f8313e37514d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 65,
                "y": 68
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b083598a-d00d-4bad-b0f1-662af5fecd08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 53,
                "y": 68
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a69f5d44-9180-4c7f-9439-b2b9bd63a868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 41,
                "y": 68
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "673e054d-936c-406e-befa-56029bb8cb42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 68
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bdeee60b-0b11-4bcf-b45f-f79cb8509b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 14,
                "y": 68
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4ef17b4b-b793-4e6a-bcbe-5d2b443b1e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5513b8af-8e2c-460a-a547-724df78c6587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 237,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e12ee17d-75ff-473c-a7d6-5c5be9337562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 226,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ecc5881e-c944-4731-87f6-b647347bae29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 217,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a0b1a36f-6edd-48cd-9b0e-45bbd4e3d462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 203,
                "y": 46
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d9815b66-0857-4143-b101-84489aee75ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 219,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fb234af9-9cb3-422f-9f2f-1fdf3b5d9958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 193,
                "y": 68
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ae38fdb8-378f-453f-8347-9c2cc8408c34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 207,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6d066c18-9e4d-4a32-a638-c24e6b2cdf69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 192,
                "y": 24
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8d96825e-0eda-4b24-90dd-31853186f1bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": -3,
                "shift": 10,
                "w": 14,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a9624212-f65a-4f98-b33f-869d69811729",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bb163426-5d3f-4e2b-82a8-93093baa4100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6983a373-86e6-45bd-90b7-ff43843f0001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "80db9116-4e8a-4d3a-aeaa-1d4223f0bda9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c19f08d3-99b1-4934-90a4-1f03380bcc69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d1f2e08b-4e56-4cd2-8511-31447b0c9874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2c1aee8e-40a6-43ab-bcbc-0c0e15384efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "74e676d8-990c-4599-8398-d38316868616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "e12796bd-cb2d-4476-bd82-0fba377b9301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6641aa17-0635-4e51-a662-f2fc73966a7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8f2e4162-aa5d-4d19-a9e4-cf1c7fd654e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f7f75088-b013-49b9-b3bd-f140099fc31b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "631f9750-4e94-48b0-b151-47384c0d3a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "43852a95-5cbe-49db-8ec9-94e37ae1143a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "63f9638b-fefb-44c2-929a-b3d9944ca4aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "220ea9aa-930c-4a36-b6eb-e1b15b6c80d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b58c8111-86cd-4e12-b777-db80225e8e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "94228a6a-1d80-4827-8e17-9f0f2316e6dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7db3c780-4195-4876-96bc-bfdbaca24971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1cc0d9a9-3fd6-4364-9596-8856c39ed0c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0a656aac-098d-4eaa-bcc0-54def88e8c4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a443d708-721b-40ba-b3a9-ed6e6406dd7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a0791305-ca75-458f-ae3a-83b33bf834a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8f89e8c6-a297-4c0f-9e24-a5c81bec9624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": -2,
                "shift": 3,
                "w": 5,
                "x": 185,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1757b7c8-c618-4d0a-acd9-e9bab63ab331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 174,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "4d127d10-d23e-4d2e-b1a6-3d0b1a0c2de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 170,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "bf8e2a97-99f0-46df-b378-e18647c4f986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3ef6c21e-a542-4430-8294-869e791aa1f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 149,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "713dc7c0-8081-4964-b00b-92e3c4f3cdb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 140,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5b5b5b64-7005-4ba3-9208-5b643096b3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2763756e-1aa1-4a92-99c9-248378185099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 121,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c61fdec9-1b1a-4548-87fe-ea32e85ec52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 113,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "92aa749a-4883-4807-9a3f-1b136e08df65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 104,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0b170ce8-766c-49c3-87bf-19260b6f34cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 94,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a1604c2a-1655-455e-a795-04b16152d7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 84,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "649bbee9-b227-40c9-af76-cf7fa4054c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 73,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c9835209-fc89-428a-993d-78a6f6deaa78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 60,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0ee07346-14e3-4bdd-8edd-18a0ae939610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 51,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7b090771-9ec1-403c-8bb9-2be818b8446c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 42,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e239b5e2-87a3-42eb-b04a-7f5dd511a5ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 31,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "47bba5d8-845a-434d-965e-3a1b6eb55c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 24,
                "y": 24
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6de661b1-2f9f-4649-b570-0a6109ccc13d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 20,
                "y": 24
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3cef5717-b010-4481-a747-a56eb346f088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 13,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d187c608-15ee-4c56-93f9-76f0c722fdf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "73ef8877-41f2-430d-9cf2-b62e636391de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 20,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 205,
                "y": 24
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "27f66d1e-8c65-4b5b-844e-6e0f4c13c629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 20,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 206,
                "y": 68
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}