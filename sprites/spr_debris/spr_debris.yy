{
    "id": "cea18be7-f192-49a8-8d95-5877a69f90ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c735baf-67f7-4703-b343-f88b0849b773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cea18be7-f192-49a8-8d95-5877a69f90ff",
            "compositeImage": {
                "id": "1b116291-295f-4b47-b7d0-48e6d3c29b12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c735baf-67f7-4703-b343-f88b0849b773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7273265a-54f0-4326-a299-b06c93dc61d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c735baf-67f7-4703-b343-f88b0849b773",
                    "LayerId": "ea0feba7-108e-4619-8c53-267252cdd458"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "ea0feba7-108e-4619-8c53-267252cdd458",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cea18be7-f192-49a8-8d95-5877a69f90ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}