{
    "id": "588b3612-c3bc-4fb1-b5b5-20940655e4d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79b3e7d1-6b67-45e0-b3a0-580aa81b4edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "588b3612-c3bc-4fb1-b5b5-20940655e4d8",
            "compositeImage": {
                "id": "99e547b0-79fd-4c31-baad-68fe273bddf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79b3e7d1-6b67-45e0-b3a0-580aa81b4edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8e32ae0-370b-45be-8cea-ed964de1d665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79b3e7d1-6b67-45e0-b3a0-580aa81b4edf",
                    "LayerId": "ed87dd61-fcc8-4d7a-9b78-7ea4715aa54f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ed87dd61-fcc8-4d7a-9b78-7ea4715aa54f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "588b3612-c3bc-4fb1-b5b5-20940655e4d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}