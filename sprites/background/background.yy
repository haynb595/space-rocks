{
    "id": "29ff8d49-8e6b-40ec-b972-1977df35f366",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 649,
    "bbox_left": 0,
    "bbox_right": 649,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da4be6d2-13a0-48ab-8338-904f6c8ef22a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29ff8d49-8e6b-40ec-b972-1977df35f366",
            "compositeImage": {
                "id": "1bdbfe38-4ad9-460f-a9d7-2f78818e32af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4be6d2-13a0-48ab-8338-904f6c8ef22a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "672e8793-39ea-4576-82a6-866a11fc6827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4be6d2-13a0-48ab-8338-904f6c8ef22a",
                    "LayerId": "e0ad068b-b9a9-48f6-8a0e-e042874a9dc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 650,
    "layers": [
        {
            "id": "e0ad068b-b9a9-48f6-8a0e-e042874a9dc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29ff8d49-8e6b-40ec-b972-1977df35f366",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 650,
    "xorig": 0,
    "yorig": 0
}