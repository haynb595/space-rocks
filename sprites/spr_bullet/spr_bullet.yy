{
    "id": "dac5c0c9-c4e1-4c45-ab72-fed1688a869d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02e81a4c-3c11-4b3e-b372-ddbd323b438e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dac5c0c9-c4e1-4c45-ab72-fed1688a869d",
            "compositeImage": {
                "id": "a350a109-5cfe-4336-a0c3-1507ed25474d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02e81a4c-3c11-4b3e-b372-ddbd323b438e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6772ed0b-9700-4ce0-a95c-29859a64e07c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02e81a4c-3c11-4b3e-b372-ddbd323b438e",
                    "LayerId": "6fa225f2-1049-496a-bf68-27c8c746ad90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "6fa225f2-1049-496a-bf68-27c8c746ad90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dac5c0c9-c4e1-4c45-ab72-fed1688a869d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 1,
    "yorig": 1
}