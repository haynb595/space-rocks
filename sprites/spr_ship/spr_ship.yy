{
    "id": "8bb5a007-265e-470b-9d83-39e83de91424",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": -1,
    "bbox_right": 32,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9eed5619-d7ab-4254-b3f5-92b9844c0c34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bb5a007-265e-470b-9d83-39e83de91424",
            "compositeImage": {
                "id": "9bb54585-e3be-4d89-98b9-de4e889a3297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eed5619-d7ab-4254-b3f5-92b9844c0c34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7d772f8-4f1c-41e5-990b-c996e50338d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eed5619-d7ab-4254-b3f5-92b9844c0c34",
                    "LayerId": "47261cde-1042-4e7c-911b-8778e26c1400"
                }
            ]
        },
        {
            "id": "5e071e3c-044a-4d79-9807-0b1b5c83b488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bb5a007-265e-470b-9d83-39e83de91424",
            "compositeImage": {
                "id": "352bb59e-34b4-431c-b87a-c2985a3fe089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e071e3c-044a-4d79-9807-0b1b5c83b488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34271e59-308b-4688-8d7f-271d1e4bf897",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e071e3c-044a-4d79-9807-0b1b5c83b488",
                    "LayerId": "47261cde-1042-4e7c-911b-8778e26c1400"
                }
            ]
        },
        {
            "id": "8da2a945-29ff-4ea7-8f3b-4679109274e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bb5a007-265e-470b-9d83-39e83de91424",
            "compositeImage": {
                "id": "7d14916e-b669-40a9-a112-9c4efb9396a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da2a945-29ff-4ea7-8f3b-4679109274e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d069f10f-d519-41eb-ab87-6354efabcbec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da2a945-29ff-4ea7-8f3b-4679109274e2",
                    "LayerId": "47261cde-1042-4e7c-911b-8778e26c1400"
                }
            ]
        },
        {
            "id": "c98c7a36-d3aa-4131-a4f4-37230f57a731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bb5a007-265e-470b-9d83-39e83de91424",
            "compositeImage": {
                "id": "313db827-2c25-4b4e-ab72-cea1cc434539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c98c7a36-d3aa-4131-a4f4-37230f57a731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ed807a-e2a9-4c39-98d1-5075860261c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c98c7a36-d3aa-4131-a4f4-37230f57a731",
                    "LayerId": "47261cde-1042-4e7c-911b-8778e26c1400"
                }
            ]
        },
        {
            "id": "bd13da73-f8a1-40fb-825e-2396cfaf9751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bb5a007-265e-470b-9d83-39e83de91424",
            "compositeImage": {
                "id": "2a05d89b-b348-4b9d-b082-54446184f4cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd13da73-f8a1-40fb-825e-2396cfaf9751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8134102f-6b7d-4aff-9a98-62bf1496ff58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd13da73-f8a1-40fb-825e-2396cfaf9751",
                    "LayerId": "47261cde-1042-4e7c-911b-8778e26c1400"
                }
            ]
        },
        {
            "id": "f8baad5e-f992-48d6-8cfc-df651c56fd0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bb5a007-265e-470b-9d83-39e83de91424",
            "compositeImage": {
                "id": "917cc076-89dd-4b54-824b-a6d44a14a2e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8baad5e-f992-48d6-8cfc-df651c56fd0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7644c20f-55ae-4d01-bcd3-83cda884d327",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8baad5e-f992-48d6-8cfc-df651c56fd0f",
                    "LayerId": "47261cde-1042-4e7c-911b-8778e26c1400"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "47261cde-1042-4e7c-911b-8778e26c1400",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bb5a007-265e-470b-9d83-39e83de91424",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}