{
    "id": "d498978e-e44a-4afa-b8cf-1cd46977e49e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd6f0735-59f5-49ab-b823-0314d71546e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d498978e-e44a-4afa-b8cf-1cd46977e49e",
            "compositeImage": {
                "id": "83d7e23d-87af-446c-b763-6ee5a9518d15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd6f0735-59f5-49ab-b823-0314d71546e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5acad27-24d7-4dd4-a531-f53bbf82c77f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd6f0735-59f5-49ab-b823-0314d71546e4",
                    "LayerId": "ed39cf82-afc8-48c0-8b8a-9339f3f8871a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ed39cf82-afc8-48c0-8b8a-9339f3f8871a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d498978e-e44a-4afa-b8cf-1cd46977e49e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}