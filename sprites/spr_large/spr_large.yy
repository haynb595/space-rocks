{
    "id": "c11d5909-fb5f-415f-a0a8-3b12bf6be268",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_large",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": -2,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9934f6b3-da7e-4a5e-8f76-9ee31bed1e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11d5909-fb5f-415f-a0a8-3b12bf6be268",
            "compositeImage": {
                "id": "14d62b95-aa0c-4b19-a0e7-a0024d31e9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9934f6b3-da7e-4a5e-8f76-9ee31bed1e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3178c443-bf0f-48fc-95b9-f6a6e652988c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9934f6b3-da7e-4a5e-8f76-9ee31bed1e77",
                    "LayerId": "f5681e98-a468-44cf-a29c-8ca822b5aec1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f5681e98-a468-44cf-a29c-8ca822b5aec1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c11d5909-fb5f-415f-a0a8-3b12bf6be268",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}