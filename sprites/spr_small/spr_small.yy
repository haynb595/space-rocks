{
    "id": "8c0ae4e5-005f-467d-b7e6-0d255a942e17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b4a8370-1cfe-41ce-a865-37a43290f5f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c0ae4e5-005f-467d-b7e6-0d255a942e17",
            "compositeImage": {
                "id": "acd16632-2b51-433e-8ee6-51008545142b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4a8370-1cfe-41ce-a865-37a43290f5f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c480aef4-2ad8-4c1b-8ccd-c4bfda73a95e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4a8370-1cfe-41ce-a865-37a43290f5f1",
                    "LayerId": "81b91929-2178-463c-b18c-42ef02043137"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "81b91929-2178-463c-b18c-42ef02043137",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c0ae4e5-005f-467d-b7e6-0d255a942e17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}