{
    "id": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c3f349e-a51e-4944-a97b-01a408a4bc91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "4c4762b0-d99c-4259-a157-c13cdfa46797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c3f349e-a51e-4944-a97b-01a408a4bc91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69f28207-44db-4f87-bd63-9afd89274162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c3f349e-a51e-4944-a97b-01a408a4bc91",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        },
        {
            "id": "93ea7732-468d-453d-9e8c-6cd3982b96cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "3b353d57-b476-42d0-979e-08a0fc36d375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93ea7732-468d-453d-9e8c-6cd3982b96cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5fa3d28-1982-4f71-a0c0-ea849b9fed9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93ea7732-468d-453d-9e8c-6cd3982b96cc",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        },
        {
            "id": "8cb6f760-ac0b-4a65-a2a8-b0e7d864cd77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "1c26c802-d1e4-4487-8cce-38b6923d37d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb6f760-ac0b-4a65-a2a8-b0e7d864cd77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "584c6d07-aa98-4b09-ab52-8a6bd90407fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb6f760-ac0b-4a65-a2a8-b0e7d864cd77",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        },
        {
            "id": "d612302b-2a96-437e-a149-4bd9f6ba0db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "414665ed-e306-463d-980c-0c3ecbccf6c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d612302b-2a96-437e-a149-4bd9f6ba0db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63275426-8533-467f-9185-b8be3a1d4127",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d612302b-2a96-437e-a149-4bd9f6ba0db2",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        },
        {
            "id": "55ac7dcd-efc6-4e5c-977f-779b0a950dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "fdcacc7b-a559-4194-8a8a-c55562a12608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ac7dcd-efc6-4e5c-977f-779b0a950dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f07c54-7850-48c2-a31b-25119ea28e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ac7dcd-efc6-4e5c-977f-779b0a950dee",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        },
        {
            "id": "f67dc8f1-ee1d-4c64-99b4-992ce8eda5c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "d3055e19-0ae6-46a2-ae1d-abfc45df6822",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f67dc8f1-ee1d-4c64-99b4-992ce8eda5c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04a76ca0-7ced-4013-94e0-f31a78c5c866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f67dc8f1-ee1d-4c64-99b4-992ce8eda5c3",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        },
        {
            "id": "7c98234e-e180-427c-9249-d16e9ea96b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "7fccea7f-c96c-44a0-959d-d27b9cfb3d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c98234e-e180-427c-9249-d16e9ea96b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "344bfbe6-5137-4aae-a6db-b597483003d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c98234e-e180-427c-9249-d16e9ea96b42",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        },
        {
            "id": "e554d4d7-7c2c-4a8e-92ff-b7a6682f477b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "79c34af0-a7cf-4709-9269-85b061d99ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e554d4d7-7c2c-4a8e-92ff-b7a6682f477b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3618283b-c38b-43f4-9ee1-e9332ba4893c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e554d4d7-7c2c-4a8e-92ff-b7a6682f477b",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        },
        {
            "id": "cc17edfa-f007-4238-a9fa-b999dad70681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "compositeImage": {
                "id": "68e222aa-c878-4215-b5f0-5af8552a2dd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc17edfa-f007-4238-a9fa-b999dad70681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ebb4c5b-0b39-49cb-8660-7d7b75c10cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc17edfa-f007-4238-a9fa-b999dad70681",
                    "LayerId": "94b7407f-34b7-491c-a672-b8a590bb4af5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "94b7407f-34b7-491c-a672-b8a590bb4af5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d8e1e41-4779-40a8-ab11-da2d2afb2734",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}